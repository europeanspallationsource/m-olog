import xml.etree.ElementTree as ET

tree = ET.parse('/etc/opt/codac-4.1/jboss/standalone.xml')
parent = tree.find("{urn:jboss:domain:1.2}profile/{urn:jboss:domain:security:1.1}subsystem/{urn:jboss:domain:security:1.1}security-domains")
secDomain = ET.SubElement(parent, 'security-domain', {'name':'olog.security-domain', 'cache-type':'default'})
auth = ET.SubElement(secDomain, 'authentication')
loginModule = ET.SubElement(auth, 'login-module', {'code':'RealmUsersRoles', 'flag':'required'})
ET.SubElement(loginModule, 'module-option', {'name':'usersProperties', 'value':'${jboss.server.config.dir}/olog-users.properties'})
ET.SubElement(loginModule, 'module-option', {'name':'rolesProperties', 'value':'${jboss.server.config.dir}/olog-roles.properties'})
ET.SubElement(loginModule, 'module-option', {'name':'realm', 'value':'olog'})
ET.SubElement(loginModule, 'module-option', {'name':'password-stacking', 'value':'useFirstPass'})
tree.write('/etc/opt/codac-4.1/jboss/standalone.xml')
