/*
 * Copyright (c) 2010 Brookhaven National Laboratory
 * Copyright (c) 2010-2011 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
 * All rights reserved. Use is subject to license terms and conditions.
 */

package edu.msu.nscl.olog;

import java.security.Principal;
import java.util.Collections;
import java.util.Set;

/**
 * This is UserManager that permits all operations. It behaves as if every user is in all groups.
 * 
 * @author Sunil Sah <sunil.sah@cosylab.com>
 */
public class PermitAllUserManager extends UserManager {

	@Override
	protected Set<String> getGroups(Principal user) {
		return Collections.<String>emptySet();
	}

	@Override
    public boolean userIsInGroup(String group) {
        return true;
    }
}
