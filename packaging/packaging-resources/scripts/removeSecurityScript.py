import xml.etree.ElementTree as ET
tree = ET.parse('/etc/opt/codac-4.1/jboss/standalone.xml')
parent = tree.find("{urn:jboss:domain:1.2}profile/{urn:jboss:domain:security:1.1}subsystem/{urn:jboss:domain:security:1.1}security-domains")
iter = parent.getiterator('{urn:jboss:domain:security:1.1}security-domain')
for i in iter:
    if i.attrib["name"] == 'olog.security-domain':
	parent.remove(i)
tree.write('/etc/opt/codac-4.1/jboss/standalone.xml')
