/*
 * Copyright (c) 2010 Brookhaven National Laboratory
 * Copyright (c) 2010-2011 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
 * All rights reserved. Use is subject to license terms and conditions.
 */

package edu.msu.nscl.olog;

import java.security.Principal;
import java.util.Collections;
import java.util.Set;

import javax.security.auth.login.LoginException;

import se.esss.ics.rbac.loginmodules.RBACPrincipal;

/**
 * This is UserManager that reads group membership from RBAC.
 * 
 * @author Sunil Sah <sunil.sah@cosylab.com>
 */
public class RBACUserManager extends UserManager {

    private ThreadLocal<RBACPrincipal> rbacUser = new ThreadLocal<RBACPrincipal>();
	
    /**
     * Checks that the provided user is RBACPrincipal and stores it.
     *
     * @param user principal, must be of type {@link RBACPrincipal} or a runtime exception is thrown.
     * @param isAdmin true if the user has admin role, else false
     */
	@Override
    public void setUser(Principal user, boolean isAdmin) {
        super.setUser(user, isAdmin);
        rbacUser.set((RBACPrincipal)user);
    }
	
	/**
	 * With RBAC, group membership check can only be performed when the group name is known, so this function always
	 * returns a dummy empty data here.
	 */
	@Override
	protected Set<String> getGroups(Principal user) {
		return Collections.<String>emptySet();
	}

	@Override
    public boolean userIsInGroup(String group) {
		final String permission = toValidPermissionName(group);
        try {
        	if (rbacUser.get().runtimePermissionExists(permission)) {
        		return rbacUser.get().hasRuntimePermission(permission);
        	} else {
        		// if permissions not assigned for the group, allow all access
        		return true;
        	}
		} catch (LoginException e) {
			throw new RuntimeException(e);
		}
    }
	
	private String toValidPermissionName(String permission) {
		// exclude all non word characters
		return permission.replaceAll("\\W", ""); 
	}
}
